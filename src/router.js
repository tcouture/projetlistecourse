import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: () => import("./views/vuetify_layout.vue"),
      children: [
        {
          path: '',
          name: "home",
          component: () => import("./views/home.vue")
        },
        {
          path: "lists",
          name: "lists",
          component: () => import("./views/lists.vue")
        },
        {
            path: "about",
            name: "about",
            component: () => import("./views/about.vue")
        },
        {
            path: "list/:id",
            name: "list",
            component: () => import("./views/list.vue")
        }
      ]
    }
  ]
});
